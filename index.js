// console.log("Lest session of the week!");

// [Section] While Loop

//A while loop takes in an expression/condition.
// Expressions are any unit of code that can be evaluated to a value.
// if the condition evaluates to be true the statements inside the code block will be executed.
// A loob will iterate a certain number of items until an expression / condition is met.
// Iteration is the term given to the repitition of statements

/*

	Syntax:
	while(expression/condition){
		statement;
		iteration;
	}

*/

// let count = 5;

// while(count!==0){
// 	console.log("While: " + count);

// 	//decrement
// 	count--;
// 	console.log("Value of count after iteration: " + count);
// }

// [Sections] do while loop
/*
	a do-while loop works alot like the while loop. but unlike while loops, do-while loops guarantee that the code will be executed once;
	Syntax:
	do{
		statement;
		iteration;
	}	
	while(expression/condition)
*/

// let number = prompt("Give me a number.");

// do{
// 	console.log("Do while: " + number);

// 	number++;
// }while(number < 10);

// [Section] For loop
/*

	-A for loop is more flexible than while and do-while.
	it consists of 3 parts:
	1. The "initialization" value that will track the progress of the loop.
	2. The "expression/condition" that will be evaluated which will determie wether the loop will run one more time.
	3. The "finalExpression" indicated how to advance the loop.

	Syntax:
	for(initialization; expression/condition; finalExpression){
		statement/statements;
	}
*/

	/*
		-we will create a loop that will start from 0 and end at 20
		-Every iteration of the loop, the value of count will be checked if it is equal or less than 20
		-if the value of count is less than or equal to 20 the statement inside of the loop will execute.
		-The value of count will be incremented by one for each iteration
	*/
	
	// for(let count = 0; count <= 20; count++){
	// 	console.log("The current value of count is: " + count);
	// }

// let myString = "alex";

	// characters in strings may be counted using the .length property
	// strings are special compare to other data types, that it has access to functions and other peices of information.

	// console.log(myString.length);

	// Accessing characters of a string.
	// Individual characters of a string may be accessed using it's index number
	//The first character in a string corresponds to [0].

	// console.log(myString[0]);
	// console.log(myString[1]);
	// console.log(myString[2]);
	// console.log(myString[3]);

	// for(let i = 0; i < myString.length; i ++){
	// 	console.log(myString[i]);
	// }

	// let numA = 15;

	// for(let i = 0; i <= 10; i++){
	// 	let exponential = numA**i;

	// 	console.log(exponential);
	// }

let myName = "alex";

	for(let i = 0; i < myName.length; i++){
		if(myName[i].toLowerCase() === "a" 
			|| myName[i].toLowerCase() === "e"
			|| myName[i].toLowerCase() === "i"
			|| myName[i].toLowerCase() === "o"
			|| myName[i].toLowerCase() === "u")
		{
			console.log(3);
		}else{
			console.log(myName[i]);
		}
	}

for(let count = 0; count <= 20; count++){
	// if remainder is equal to 0;
	if (count % 2 ===0){
		continue;
	}else if(count > 10){
		break;
	}

	console.log("continue and break: " + count);
}

let name = "alexandro";

for(let i = 0; i < name.length; i++){
	
	if(name[i] === "a"){
		console.log("Continue to next iteration.");
		continue;
	}

	if (name[i].toLowerCase() === "d") {
		console.log("Console log before break");
		break;
	}

	console.log(name[i]);

}